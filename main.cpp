//4_2. Порядковые статистики
//Дано число N и N строк. Каждая строка содержащит команду добавления или удаления натуральных чисел,
//а также запрос на получение k-ой порядковой статистики.
//Команда добавления числа A задается положительным числом A,
//команда удаления числа A задается отрицательным числом “-A”.
//Запрос на получение k-ой порядковой статистики задается числом k.
//Требуемая скорость выполнения запроса - O(log n).

#include <iostream>
#include "assert.h"
#include <fstream>

using std::ifstream;
using std::ofstream;

struct Node
{
    int value;
    int height;
    int countNods;
    Node * left;
    Node * right;
    Node( const int & value ): value( value ), height(1), countNods(1), left( nullptr ), right( nullptr ){};
};

template < class T, class Compare >
class TreeAVL
{
public:
    TreeAVL( const Compare & cmp );
    TreeAVL( const TreeAVL & cmp ) = delete;
    TreeAVL& operator = ( const TreeAVL & cmp ) = delete;
    ~TreeAVL();

    void Add( const T & value );
    void Del( const T & value );
    T SearchKStat( int kStat );

private:
//    void deleteSpecificNode( Node *& node, bool isLeft );
    Node* deleteNodeLeft( Node *& node );
    Node* deleteNodeRight( Node *& node );
    void deleteNode( Node *& tree, T value );
    T searchKStat( Node * node, int kStat );
    void addNode( Node *& node, T value );
    void smallRotateRight( Node *& tree );
    void smallRotateLeft( Node *& tree );
    int getCurrentBalance( Node * tree );
    void bigRotateRight( Node* & tree);
    void bigRotateLeft (Node* & tree );
    void destroyTree( Node * node );
    Node* fixBalance( Node *& tree );
    void rotateRight( Node* & tree );
    void rotateLeft( Node* & tree );
    void fixHeight( Node * tree );
    void fixCount( Node * tree );
    int height( Node * tree );
    int count( Node * tree );

    Node * root;
    Compare cmp;
};

template < class T, class Compare >
TreeAVL< T, Compare >::TreeAVL( const Compare &cmp ) : cmp( cmp ), root( nullptr )
{
}

template < class T, class Compare >
TreeAVL< T, Compare >::~TreeAVL()
{
    destroyTree( root );
}

template < class T, class Compare >
void TreeAVL< T, Compare >::Add( const T &value )
{
    addNode(root, value );
}

template < class T, class Compare >
void TreeAVL< T, Compare >::Del( const T &value )
{
    deleteNode(root, value);
}

template < class T, class Compare >
T TreeAVL< T, Compare>::SearchKStat( int kStat )
{
    return searchKStat( root, kStat );
}

//private methods
template< class T, class Compare >
T TreeAVL< T, Compare >::searchKStat( Node * node, int kStat )
{
    assert( node );
    int leftCount = count( node->left );
    if( leftCount == kStat )
        return node->value;
    else if( leftCount > kStat )
        return searchKStat(node->left, kStat);
    else
        return searchKStat(node->right, kStat - leftCount - 1);
}

template < class T, class Compare >
int TreeAVL< T, Compare >::height( Node * tree )
{
    return !tree ? 0 : tree->height;
}

template < class T, class Compare >
int TreeAVL< T, Compare >::count( Node * tree )
{
    return !tree ? 0 : tree->countNods;
}

template < class T, class Compare >
int TreeAVL< T, Compare >::getCurrentBalance( Node * tree )
{
    return tree != nullptr ? height(tree->right)-height( tree->left ) : 0;
}

template < class T, class Compare >
void TreeAVL< T, Compare >::fixCount( Node * tree )
{
    if(!tree)
        return;

    int right = count(tree->right);
    int left = count(tree->left);
    tree->countNods = left + right + 1;
}

template < class T, class Compare >
void TreeAVL< T, Compare >::fixHeight( Node * tree )
{
    if(!tree)
        return;

    int right= height(tree->right);
    int left = height(tree->left);
    tree->height=right>left ? right+1 : left+1;
}
///////////////////////////
template < class T, class Compare >
void TreeAVL< T, Compare >::smallRotateRight( Node*&tree )
{
    Node*newTree=tree->left;
    tree->left=newTree->right;
    newTree->right=tree;
    fixHeight(tree);
    fixHeight(newTree);
    fixCount(tree);
    fixCount(newTree);
    tree=newTree;
}

template < class T, class Compare >
void TreeAVL< T, Compare >::smallRotateLeft( Node *& tree )
{
    Node*newTree=tree->right;
    tree->right=newTree->left;
    newTree->left=tree;
    fixHeight(tree);
    fixHeight(newTree);
    fixCount(tree);
    fixCount(newTree);
    tree=newTree;
}

template < class T, class Compare >
void TreeAVL< T, Compare >::bigRotateLeft( Node* & tree )
{
    smallRotateRight(tree->right);
    smallRotateLeft(tree);

}

template < class T, class Compare >
void TreeAVL< T, Compare >::bigRotateRight( Node* & tree )
{
    smallRotateLeft(tree->left);
    smallRotateRight(tree);

}

template < class T, class Compare >
void TreeAVL< T, Compare >::rotateLeft( Node* & tree )
{
    if (!tree)
        return;
    if (getCurrentBalance( tree->right )==-1)
        bigRotateLeft(tree);
    else
        smallRotateLeft(tree);
}

template < class T, class Compare >
void TreeAVL< T, Compare >::rotateRight( Node* & tree )
{
    if (!tree)
        return;
    if (getCurrentBalance(tree->left)==1)
        bigRotateRight(tree);
    else
        smallRotateRight(tree);
}

template < class T, class Compare >
Node* TreeAVL< T, Compare>::fixBalance( Node *& tree )
{
    fixHeight(tree);
    fixCount(tree);
    if (getCurrentBalance(tree)==-2)
        rotateRight(tree);
    else if (getCurrentBalance(tree)==2)
        rotateLeft(tree);
    return tree;
}
/////////////////////////////////////////////

//template < class T, class Compare >
//void TreeAVL< T, Compare >::deleteSpecificNode( Node *& node, bool isLeft )
//{
//    if( node->left == nullptr ) //когда левое пустое
//    {
//        Node * tmp = node;
//        node = node->right;
//        delete tmp;
//    }
//    else if( node->right == nullptr ) //когда правое пустое
//    {
//        Node * tmp = node;
//        node = node->left;
//        delete tmp;
//    }
//    else if ( isLeft )  //когда в левом поддереве ищем макс
//    {
//        Node * minParent = node;
//        Node * min = node->left;
//        while( min->right != nullptr )
//        {
//            minParent = min;
//            min = min->right;
//        }
//        Node * tmp1 = node;
//        if( node->left == min )
//        {
//            node = min;
//            node->right = tmp1->right;
//            delete tmp1;
//        }
//        else
//        {
//            minParent->right = min->left;
//            min->left = node->left;
//            node = min;
//            node->right = tmp1->right;
//            delete  tmp1;
//        }
//    }
//    else //когда в правом поддереве ищем мин
//    {
//        Node * minParent = node;
//        Node * min = node->right;
//        while( min->left != nullptr )
//        {
//            minParent = min;
//            min = min->left;
//        }
//        Node * tmp1 = node;
//        if( node->right == min )
//        {
//            node = min;
//            node->left = tmp1->left;
//            delete tmp1;
//        } else
//        {
//            minParent->left = min->right;
//            min->right = node->right;
//            node = min;
//            node->left = tmp1->left;
//            delete  tmp1;
//        }
//    }
//}
template < class T, class Compare >
Node* TreeAVL<T,Compare>::deleteNodeLeft( Node*& node )
{
    if(!node)
        return nullptr;
    if( !node->right )
    {
        Node * currentNode = node;
        node = node->left;
        return  currentNode;
    }
    else
    {
        Node * current = deleteNodeLeft(node->right);
        fixBalance(node);
        return current;
    }
}

template < class T, class Compare >
Node* TreeAVL<T,Compare>::deleteNodeRight( Node*& node )
{
    if(!node)
        return nullptr;
    if( !node->left )
    {
        Node * currentNode = node;
        node = node->right;
        return currentNode;
    }
    else
    {
        Node * current = deleteNodeRight(node->left);
        fixBalance(node);
        return current;
    }
}

template < class T, class Compare >
void TreeAVL< T, Compare >::deleteNode( Node *& tree, T value )
{
    if( !tree )
        return;
    if( !cmp(tree->value, value) && !cmp(value, tree->value) )
    {
        bool isLeft = height(tree->left) > height(tree->right);
        Node * newTree, *tmp;
        if( isLeft )
        {
            newTree = deleteNodeLeft(tree->left);
            if( newTree )
            {
                newTree->right = tree->right;
                newTree->left = tree->left;
            }
        }
        else
        {
            newTree = deleteNodeRight(tree->right);
            if( newTree )
            {
                newTree->left = tree->left;
                newTree->right = tree->right;
            }
        }
        tmp = tree;
        tree = newTree;
        delete tmp;
        fixBalance(tree);
        return;
    }
    deleteNode( cmp(tree->value, value) ? tree->left : tree->right, value);
    fixBalance( tree);
}

template < class T, class Compare >
void TreeAVL< T, Compare>::destroyTree( Node * node )
{
    if( !node )
        return;
    if( node->left )
        destroyTree( node->left );
    if( node->right )
        destroyTree( node->right );

    delete node;
}

template < class T, class Compare >
void TreeAVL< T, Compare>::addNode( Node *& node, T value )
{
    if( !node )
    {
        node = new Node( value );
        return;
    }

    if( cmp(value, node->value) )
        addNode( node->right, value );
    else
        addNode( node->left, value );

    fixBalance( node );
    fixCount( node );
}

template < class T >
struct CompareDefault
{
    bool operator () ( const T & leftValue, const T & rightValue )
    {
        return leftValue > rightValue;
    }
};

int main()
{
    ifstream fin("input.txt");
    ofstream fout("output.txt");
    if( !fin.is_open() && !fout.is_open() )
        return -1;

    CompareDefault<int> compare;
    TreeAVL< int, CompareDefault<int> > tree( compare );
    int countOperation, value, kStat;
    fin >> countOperation;
    for( int i = 0; i < countOperation; ++i )
    {
        fin >> value >> kStat;
        if( value >= 0 )
            tree.Add(value);
        else
            tree.Del(abs(value));

        fout << tree.SearchKStat(kStat) << " ";
    }

    fout.close();
    fin.close();
    return 0;
}
